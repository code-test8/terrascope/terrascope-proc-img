# Setup on MacOS
- Install Docker Desktop
- Create the following folders
    - /tmp/terrascope-storage/uploads
    - /tmp/terrascope-storage/reads
- Install Redis container using the following command
    ```bash
    docker run --name redis -d redis -v /Users/njt/docker.data/redis:/data -p 6379: 6379
    ```
- install package for the respective apps and services

# terrascope-api-server
A RESTful api server to upload, preview and download the images
| API               | METHOD      | Description         |
| -----------       | ----------- | -----------         |
| /api/ping         | GET         |                     |
| /api/images/upload| POST        |                     | 
| /api/images/:id   | GET         |  original jpeg      |
| /api/images/:id   | GET         |  ?type=thumbnail - Only thumbnail and jpeg |
| /api/images/:id   | GET         |  ?ext=webp - Only extension |
| /api/images/:id   | GET         |  ?ext=webp&type=thumbnail - extension and type |
| /api/images/:id   | GET         |  ?download - Download original file |

```bash
npm run test
npm run debug
npm run start
```

# terrascope-image-processor
Using sharp library using image message from BullMQ's `image-proc` is processed by `SingletonImageProcConsumer` and created 3 different files  
<ol>
    <li>original.jpeg</li>
    <li>thumbnail.jpeg</li>
    <li>thumbnail.webp</li>
</ol>

```bash
npm run debug
npm run start
```

# terrascope-web-client
A vue application to demonstrate image upload and preview (under progress)

## WorkFlow
When user uploads the image, the `multer` middleware check file-type, file-size before the callback reaches controller action.   

The `uploadImage` action puts the file into `</dir>/uplods` directory and sends out a signal inform of a queue message into `image-proc` queue. The solution is taking the advantage of `BullMQ` for light speed and fault-tolerance processing.   

`SingletonImageProcConsumer` in the worker, pulls the upload path from the message data, and creates 3 different images into `</dir>/reads` directory.

# Docker Compose
Though the each service are contained in their repsective folders, when it becomes containerisation and communication they all needs to be started under one bridge network

```bash
$ cd terrascope
$ docker-compose up
```
### Dependency
`terrascope-api-server`, `terrascope-image-processor` => `redis-server`  
`terrascope-api-server` => `terrascope-image-processor`  
`terrascope-web-client` => `terrascope-api-server`

# NOTE: 
`sharp` module in `terrascope-image-processor` has an issue while building the docker via docker-compose. Could not able to resolve at the moment. However the other two services are running 