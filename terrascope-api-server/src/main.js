const cors = require("cors");
const fs = require("fs");
const express = require("express");
const app = express();
const router = require("./router");
const exceptionMiddleware = require("./middlewares/exception.mw");
const logger = require("./common/logger")("main");
const main = () => {
    app.set("trust proxy", true);
    app.disable('x-powered-by');
    app.use(cors());
    app.use(express.json({ limit: "10mb" }));
    app.use(express.urlencoded({ limit: "10mb", extended: true }));
    router(app);

    app.use(exceptionMiddleware);
    verifyDirExists();

    return app;
}

const verifyDirExists = () => {
    if(!fs.existsSync(process.env.IMAGE_UPLOAD_DIR)) {
        logger.error(`Upload dir (${process.env.IMAGE_UPLOAD_DIR}) does not exist`);
        process.exit(1);
    }
    if(!fs.existsSync(process.env.IMAGE_READ_DIR)) {
        logger.error(`Read dir (${process.env.IMAGE_READ_DIR}) does not exist`);
        process.exit(1);
    }
}

module.exports = main;