const { Queue } = require("bullmq");
const { redisConfig } = require("../../../redis/config");

const SingletonImageProcProducer = (function () {
    let instance;

    function createInstance() {
        return new Queue("image-proc", { connection: redisConfig(false).redis });;
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();

module.exports = SingletonImageProcProducer;