const path = ".env.test";
const fs = require("fs");
require("dotenv").config({ path });

const request = require("supertest");
const app = require("../src/main")();
const testJpegImage = __dirname + "/test-images/test.jpeg";
const testWebPImage= __dirname + "/test-images/test.webp";
const testBigJpegImage = __dirname + "/test-images/big.jpeg";
let uploadedFileName = null;

afterAll(done => {
    if(uploadedFileName){
        const desinationPath = process.env.IMAGE_UPLOAD_DIR + "/" + uploadedFileName;
        if(fs.existsSync(desinationPath)) {
            fs.unlinkSync(desinationPath);
        }
    }
    done()
})

describe("GET /api/ping", () => {
    it("should return server info", async () => {
        const res = await request(app).get("/api/ping");
        expect(res.statusCode).toBe(200);
        expect(res.body.server_name).toBeDefined();
        expect(res.body.server_time).toBeDefined();
    });
});


describe("GET /api/images", () => {
    it("should upload image", async () => {
        const res = await request(app).post('/api/images/upload').attach('image', testJpegImage);
        const { originalname, mimetype, filename } = res.body;
        expect(res.statusCode).toBe(200);
        expect(res.body.filename).toBeDefined();
        expect(res.body.originalname).toBeDefined();
        expect(res.body.originalname).toEqual("test.jpeg")
        uploadedFileName = res.body.filename;
    });

    it("uploaded file should exist in the desination", async () => {
        const desinationPath = process.env.IMAGE_UPLOAD_DIR + "/" + uploadedFileName;
        expect(fs.existsSync(desinationPath)).toEqual(true)
    })

    it("should fail for .webp", async () => {
        const res = await request(app).post('/api/images/upload').attach('image', testWebPImage);
        expect(res.statusCode).toBe(500);    
        expect(res.body.status).toBeDefined();   
        expect(res.body.status).toEqual("error")
    })

    it(`should fail file size greater than ${process.env.IMAGE_SIZE_LIMIT}MB`, async () => {
        const res = await request(app).post('/api/images/upload').attach('image', testBigJpegImage);
        expect(res.statusCode).toBe(500);    
        expect(res.body.status).toEqual("error");
        expect(["FILE_SIZE_EXCEEDED", "LIMIT_FILE_SIZE"]).toContain(res.body.code);
    })
});