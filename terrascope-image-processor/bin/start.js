const path = (!process.env.APP_ENV || process.env.APP_ENV === 'local') ? '.env' : `${process.env.APP_ENV}.env`;
require("dotenv").config({ path });

const redis = require("../src/redis");
const imageProcConumser = require("../src/queue/consumers/image-proc.consumer");

(async () => {
    await redis.init();
    await imageProcConumser.init();
    console.log("Image Processor Worker Started");
})();