function logger(module_name) {
    const timestamp = () => `[${module_name} ${(new Date()).toISOString()}]:`;
    return {
        debug(...args) {
            args.forEach(x => console.debug(timestamp() + x));
        },
        log(...args) {
            args.forEach(x => console.log(timestamp() + x));
        },
        info(...args) {
            args.forEach(x => console.info(timestamp()+ x));
        },
        warn(...args) {
            args.forEach(x => console.warn(timestamp() + x));
        },
        error(...args) {
            args.forEach(x => console.error(timestamp() + x));
        },
        table(json) {
            console.table(json);
        }
    }
}
module.exports = (module_name) => {
    return logger(module_name);
}