const multer = require('multer');
const fileTypes = process.env.IMAGE_FILE_TYPES.split(';');
const logger = require("../common/logger")("image.mw")

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, (process.env.IMAGE_UPLOAD_DIR || "/tmp/terrascope-storage/uploads"))
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, file.fieldname + '-' + uniqueSuffix)
  },
})

const imageUploader = multer({
  storage: storage,
  limits: {
    fileSize: parseInt(process.env.IMAGE_SIZE_LIMIT) * 1024 * 1024,
  },
  fileFilter: (req, file, cb) => {
    // logger.debug(`File type: ${file.mimetype}`);
    if (fileTypes.indexOf(file.mimetype) >= 0) {
      return cb(null, true);
    }
    return cb(new Error(`only ${fileTypes.map(x => '.' + x.split('/')[1]).join(',')} are allowed`));
  }
});
module.exports = imageUploader;