const { Worker } = require("bullmq");
const sharp = require('sharp');
const fs = require('fs');
const { redisConfig } = require("../../redis/config");
const logger = require("../../common/logger")("image-proc-consumer");
const qname = "image-proc";

const SingletonImageProcConsumer = (function () {
    let instance;

    function createInstance() {
        instance = new Worker(qname, jobHandler, { concurrency: 10, lockDuration: 1000, connection: redisConfig(false).redis });
        instance.on('completed', job => {
            logger.debug(`${qname}:${job.id} has completed!`);
        });
        instance.on('failed', (job, err) => {
            logger.error(`${qname}:${job.id} has failed with ${err.message}`);
        });
    }

    return {
        getInstance: function () {
            return instance;
        },
        init: function () {
            if (!instance) {
                instance = createInstance();
            }
            logger.debug(`${qname}-consumer:Initialized`);
            return instance;
        },
    };
})();

async function jobHandler(job) {
    logger.debug(`${qname}:${job.id} has started!`);
    const { id, name, data, timestamp, queueName } = job;
    const { path, filename, originalname } = data;
    const extension = originalname.split(".")[1];
    const newFolder = process.env.IMAGE_READ_DIR + `/${filename}`;
    
    try {
        if (!fs.existsSync(newFolder)) {
            fs.mkdirSync(newFolder);
            logger.debug(`${newFolder} created successfully.`);
        }
        //AS-IS
        await sharp(path).toFile(`${newFolder}/original.${extension}`);
        //THUMBNAIL JPEG
        await sharp(path).resize(200, 200).jpeg({ quality: 50}).toFile(`${newFolder}/thumbnail.jpeg`);
        //THUMBNAIL WEBP
        await sharp(path).resize(200, 200).jpeg({ quality: 50}).toFile(`${newFolder}/thumbnail.webp`);

        //REMOVE FILE
        fs.unlinkSync(path);
    } catch (e) {
        logger.error(`{qname}:Job Error`, e);
    }
}

module.exports = SingletonImageProcConsumer;