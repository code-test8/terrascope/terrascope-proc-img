const logger = require("../common/logger")("middleware:exception")

function errorMiddleware (error, req, res, next) {
  try {
    const status = error.status || 500;
    let message = error.message || 'Something went wrong';
    let code = error.code || undefined;
    let stack = undefined;
    let errorName = error?.name?.toLowerCase() || "unknown";
    if (errorName == "typeerror") {
      message = "Something went wrong while processing this request";
      logger.error(`ErrorName: ${error.name}\nErrorStack: ${error.stack}`);
    }
    if (process.env.APP_ERR_STACK == '1') {
      stack = error?.stack || undefined;
      if(error.actualException) {
        stack = stack + ";" + error.actualException?.stack
      }
    }
    res.status(status).json({ status: "error", message, code, stack });
  } catch (error) {
    next(error);
  }
};
 
module.exports = errorMiddleware;
