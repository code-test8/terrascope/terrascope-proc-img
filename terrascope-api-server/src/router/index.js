const express = require("express");
const multer  = require('multer')
const imageUploaderMiddleware = require("../middlewares/image.mw");
const defaultRouter = express.Router();
const { ping } = require("../api/default.controller");
const { uploadImage, getImage } = require("../api/image.controller");

defaultRouter.get("/ping", ping);
defaultRouter.post("/images/upload", imageUploaderMiddleware.single("image"), uploadImage);
defaultRouter.get("/images/:image_id", getImage);

const init = (app) => {
    [defaultRouter
    ]
        .forEach((router) => {
            app.use("/api", router);
        });
};

module.exports = init;
