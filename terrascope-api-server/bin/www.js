const path = (!process.env.APP_ENV || process.env.APP_ENV === 'local') ? '.env' : `${process.env.APP_ENV}.env`;
require("dotenv").config({ path });

const main = require("../src/main");
const port = process.env.PORT || process.env.APP_PORT;
const redis = require("../src/redis");

(async () => {
    await redis.init();
    const app = main();
    app.listen(port, () => {
        console.log(`Traditio API Server listening on http://localhost:${port}/api`)
    })
})();