const redisConfig = (addKeyPrefix = true) => {
    return {
        redis: {
            host: process.env.REDIS_HOST,
            port: parseInt(process.env.REDIS_PORT || '6379'),
            password: process.env.REDIS_PASSWORD,
            name: process.env.REDIS_NAME,
            keyPrefix: (addKeyPrefix === true) ? (process.env.REDIS_KEY_PREFIX || "TERRASCOPE:") : undefined,
            db: parseInt(process.env.REDIS_DATA_INDEX || "0"),
        }
    }  
}

module.exports = {
    redisConfig
}