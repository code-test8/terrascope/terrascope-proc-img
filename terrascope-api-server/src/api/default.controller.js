function ping(req, res, next) {
    return res.json({
        server_time: Date.now(),
        server_name: process.env.APP_NAME
    })
}

module.exports = { ping };