const SingletonImageProcProducer = require("../services/queue/producers/image-proc.producer");
const fs = require('fs');

async function uploadImage(req, res, next) {
    const { originalname, filename, size, mimetype } = req.file;
    await SingletonImageProcProducer.getInstance().add("image-proc", req.file)
    return res.json({
        originalname, filename, size, mimetype
    });
}

async function getImage(req, res, next) {
    const image_id = req.params.image_id;
    const dir = process.env.IMAGE_READ_DIR + `/${image_id}`;
    const ext = req.query.ext || 'jpeg';
    const type = req.query.type || 'original';
    const path = `${dir}/${type.toLowerCase()}.${ext.toLowerCase()}`;
    const access_type = req.query.hasOwnProperty('download') ? '1' : null;
    if (fs.existsSync(path)) {
        if(access_type) {
            return res.download(path);
        }
       return res.sendFile(path);
    }
    return res.status(400).json({
        message: "file you are requesting is not found",
        code: "FILE_NOT_FOUND",
        status: "error"
    })
}

module.exports = {
    uploadImage,
    getImage
}