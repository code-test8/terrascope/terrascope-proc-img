const ioredis = require("ioredis");
const { redisConfig } = require("./config");
const redis = new ioredis(redisConfig().redis);
const logger = require("../common/logger")("redis");

redis.init = async function () {
    try {
        await redis.ping();
        logger.log(`Redis Connected`);
    } catch (e) {
        logger.error(`Redis failed to connect`, e);
        process.exit(1);
    }
}
module.exports = redis